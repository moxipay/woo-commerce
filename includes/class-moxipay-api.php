<?php
/**
 * Created by PhpStorm.
 * User: e-36
 * Date: 12/27/2017
 * Time: 11:25 AM
 */

class MoxipayApi {

	private $client_id;
	private $client_secret;
	private $username;
	private $password;
	private $environment;
	private $autodeposit;
	private $gateway_id;
	public $default_error;
	private $default_secret_answer;
	public $id;

	const API_URL = 'https://api.moxipay.com';
	const SANDBOX_API_URL = 'http://testapi.moxipay.com';
	const TEXT_DOMAIN = 'moxipay-emt-gateway';

	public function __construct($gateway_id, $client_id, $client_secret, $username, $password, $environment, $autodeposit, $default_secret_answer) {

		$this->gateway_id = $gateway_id;
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
		$this->username = $username;
		$this->password = $password;
		$this->environment = $environment;
		$this->autodeposit = $autodeposit;
		$this->default_secret_answer = $default_secret_answer;
		$this->default_error = __('We are currently experiencing problems trying to place your order. Please email '. get_option('admin_email') .' for assistance.',
			self::TEXT_DOMAIN);

	}

	public function getAccessToken($sslverify = false)
	{

		$loginVars = array(
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'username' => $this->username,
			'password' => $this->password,
			'grant_type' => 'password',
			'scope' => 'merchant',
		);

		$response = wp_remote_post($this->getUrl() . '/oauth/access_token', array(
			'method' => 'POST',
			'body' => http_build_query($loginVars),
			'timeout' => 60,
			'sslverify' => $sslverify,
		));

		if (is_wp_error($response) || empty($response['body'])) {

			$log = isset($response->errors)?Moxipay_Payment_Gateway_IPN::format_response($response->errors):'Empty body';

			WC_Moxipay_EMT_Gateway::log( 'Token Request Failed: ' .$log);

			return new WP_Error( 'error',$this->default_error );
		}

		$objectResponse = $response['body'];

		if (isset($objectResponse->error)){

			WC_Moxipay_EMT_Gateway::log( 'Token Request Failed: ' . $objectResponse->error_description);

			return new WP_Error( 'error',$this->default_error );
		}


		return json_decode($objectResponse);

	}

	public function createInvoice($data,$accessToken,$order_id,$sslverify = false){
		$order = new WC_Order( $order_id );
		//$question = sanitize_text_field($data[$this->gateway_id.'_question']);
		//$answer = sanitize_text_field($data[$this->gateway_id.'_answer']);
		//$uniqueId = sanitize_text_field($data[$this->gateway_id.'_note']);
		$question = 'my favourite item';
		//$answer = 'vitamins';
		//$answer = sprintf( "%06d" , $order->get_id() );

		if ( isset( $this->default_secret_answer ) && $this->default_secret_answer != '' )
			$answer = $this->default_secret_answer;
		else
			$answer = substr( hexdec( uniqid() ), - 6 );
		
		//$uniqueId = uniqid();
		$uniqueId = substr( uniqid(),0,10 );
		$user = wp_get_current_user();

		$payload = array(
			'access_token'  	=> $accessToken,
			'invoice'       	=> str_replace( "#", "", $order->get_order_number() ),
			'amount'        	=> $order->get_total(),
			'question'          => $question,
			'answer'          	=> $answer,
			'uniqueid'       	=> $uniqueId,
			'customername'     	=> $order->get_formatted_billing_full_name(),
			'customeremail'     => $order->get_billing_email(),
			'ipnurl'			=> add_query_arg( 'wc-api', 'wc_gateway_moxipay_emt_ipn', home_url( '/' ) ),
		);

		if("no" == $this->autodeposit ){

			$payload['question'] = $question;
			$payload['answer'] = $answer;
		}

		if($user->exists()){

			$payload['userid'] = $user->ID;

		}else{

			$payload['userid'] = $order->get_billing_email();
		}

		/*
		* Documentation at this time said ipnurl is required only for development purposes, but it needs to be sent in live environments as well
		*/
		/*if("yes" == $this->environment) {

			$payload['ipnurl'] =  add_query_arg( 'wc-api', 'wc_gateway_moxipay_emt_ipn', home_url( '/' ) );
		}*/

		$response = wp_remote_post( $this->getUrl().'/merchant/populateemtpayment', array(
			'method'    => 'POST',
			'body'      =>  http_build_query($payload),
			'timeout'   => 60,
			'sslverify' => $sslverify,
		) );


		if (is_wp_error($response)) {

			$log = Moxipay_Emt_Gateway_IPN::format_response($response->errors);

			WC_Moxipay_EMT_Gateway::log( 'Invoice Request Failed: ' .$log);

			return new WP_Error( 'error',$this->default_error );
		}

		if("no" == $this->environment &&  200 == $response['response']['code'] && empty($response['body'])){

			$this->save_order_meta($order,$payload);

		}elseif("yes" == $this->environment) {

			$var = parse_url($response['body']);
			//When in test mode, we want to be able to save uniqueid, question, answer, etc
			$this->save_order_meta($order,$payload);

			if(isset($var['query'])) {
				parse_str( $var['query'], $query );

				$order->add_order_note($response['body']);
				WC_Moxipay_EMT_Gateway::log($response['body']);

				if(isset( $query['emtid']) && !empty($query['emtid'])){

					//$this->save_order_meta($order,$payload);
					update_post_meta( $order->get_id(), '_moxipay_emt_id', $query['emtid'] );
				}
			}


		}else{

			$error_msg = $this->default_error;

			if(!empty($response['body'])){

				$body_json = json_decode($response['body'],true);

				if(json_last_error() === JSON_ERROR_NONE){

					WC_Moxipay_EMT_Gateway::log( $body_json['error'].': ' .$body_json['error_message']);

				}else{

					WC_Moxipay_EMT_Gateway::log( 'error: TEST' .$response['body'] .  ' id: ' . $this->gateway_id . ' client: ' . $this->client_id  . ' sec: ' . $this->client_secret . ' user: ' . $this->username . ' pass: ' . $this->password  . ' env: ' . $this->environment . ' auto: ' . $this->autodeposit);
					$error_msg = 'Moxipay EMT - Missing required field TEST';

				}

			}

			return new WP_Error( 'error', $error_msg  );
		}

		return true;
	}

	public function save_order_meta(WC_Order $order, $payload){

		set_transient('order_'.$order->get_id().'_question',$payload['question'], 60 * 60 * 24 * 2);
		set_transient('order_'.$order->get_id().'_answer',$payload['answer'], 60 * 60 * 24 * 2);
		set_transient('order_'.$order->get_id().'_uniqueid',$payload['uniqueid'], 60 * 60 * 24 * 2);
		update_post_meta( $order->get_id(), '_moxipay_emt_uniqueid', $payload['uniqueid'] );
		update_post_meta( $order->get_id(),'_moxipay_emt_status','pending');
		update_post_meta($order->get_id(),'_moxipay_emt_customername',$payload['customername']);
		update_post_meta($order->get_id(),'_moxipay_emt_customeremail',$payload['customeremail']);
		update_post_meta($order->get_id(),'_moxipay_emt_userid',$payload['userid']);
		update_post_meta( $order->get_id(), '_moxipay_emt_answer', $payload['answer'] );

	}

	public function remove_order_meta(WC_Order $order){

		delete_transient('order_'.$order->get_id().'_question');
		delete_transient('order_'.$order->get_id().'_answer' );
		delete_transient('order_'.$order->get_id().'_uniqueid' );
	}

	private function getUrl(){
		// Decide which URL to post to
		$environment_url = self::API_URL ;
		if("yes" == $this->environment) {
			$environment_url = self::SANDBOX_API_URL;
		}
		return $environment_url;
	}


}