<?php

/**
 * @since      1.0.0
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/includes
 * @author     ecreations <josel@ecreations.net>
 */

class WC_Moxipay_EMT_Gateway extends WC_Payment_Gateway
{
    /** @var bool Whether or not logging is enabled */
    public static $log_enabled = false;

    public static $log = false;
    public static $gateway_id = "moxipay_emt";
	const TEXT_DOMAIN = 'moxipay-emt-gateway';
    public  $default_error;


// Setup our Gateway's id, description and other values
    function __construct() {

        // The global ID for this Payment method
        $this->id = self::$gateway_id;

        // The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
        $this->method_title = __( "Interac eTransfer (MoxiPay)", self::TEXT_DOMAIN );

        // The description for this Payment Gateway, shown on the actual Payment options page on the backend
        $this->method_description = __( "MoxiPay Interac eTransfer Payment Gateway Plug-in for WooCommerce", self::TEXT_DOMAIN );

        // The title to be used for the vertical tabs that can be ordered top to bottom
        $this->title = __( "Interac eTransfer (MoxiPay)", self::TEXT_DOMAIN );
		$this->environment = $this->get_option( 'environment' );
        // If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
        $this->icon = plugin_dir_url( __FILE__ ) . 'assets/img/etransfer.png';

        // Bool. Can be set to true if you want payment fields to show on the checkout
        // if doing a direct integration, which we are doing in this case
        $this->has_fields = false;

        //$this->order_button_text  = __( 'Proceed to MoxiPay EMT', 'woocommerce' );

        // This basically defines your settings which are then loaded with init_settings()
        $this->init_form_fields();
        $this->init_settings();

		// Turn these settings into variables we can use
	    foreach ($this->settings as $setting_key => $value) {
		    $this->$setting_key = $value;
	    }

	    $this->init_payment_fields();

	    $this->api = new MoxipayApi( 
			$this->id, 
			$this->client_id, 
			$this->client_secret, 
			$this->username, 
			$this->password, 
			$this->environment, 
			$this->autodeposit, 
			$this->default_secret_answer
		);

        $this->default_error =  $this->api->default_error;

        self::$log_enabled    = $this->debug;

        // Lets check for SSL
        add_action( 'admin_notices', array( $this,	'do_ssl_check' ) );

        // Save settings
        if ( is_admin() ) {
            // Versions over 2.0
            // Save our administration options. Since we are not going to be doing anything special
            // we have not defined 'process_admin_options' in this class so the method in the parent
            // class will be used instead
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        }


        if($this->enabled = 'yes'){

            new Moxipay_Emt_Gateway_IPN($this->debug,$this->settings,$this->api);
        }
		// display instructions on order thank you page
		add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ), 10, 1 );
		// display instructions on order view page
		add_action( 'woocommerce_order_details_after_order_table', array( $this, 'etransfer_vieworder_page' ), 10, 1 );
		// add instructions to Customer Emails
		add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
    } // End __construct()


		/**
		* Output for the order received page.
		*/
		public function thankyou_page($order_id) {
			$order = wc_get_order($order_id);
			$order_number = $order->get_id();
			$uniqueid = get_post_meta( $order_number, '_moxipay_emt_uniqueid', true );
			$answer = get_post_meta( $order_number, '_moxipay_emt_answer', true );
			//echo '<p>ANSWER: <strong>' . $answer . '</strong></p>';
			//echo '<p>Unique ID: <strong>' . $uniqueid . '</strong></p>';

			$instructions_autodeposit = $this->autodeposit_desc;
			$instructions_autodeposit = str_replace( "{recipient_email}", $this->recipient_email , $instructions_autodeposit );
			$instructions_autodeposit = str_replace( "{uniqueid}", $uniqueid , $instructions_autodeposit );
			$instructions_autodeposit = str_replace( "{answer}", $answer , $instructions_autodeposit );
			echo  wpautop( wptexturize( $instructions_autodeposit ) );
			echo '<br/>';
			echo '<p><a target="_blank" class="button alt call-to-action" href="'.wc_get_endpoint_url( 'view-order', $order_id, wc_get_page_permalink( 'myaccount' )).'">View Order</a></p>';
		}
		
		/**
		 * Output for the view order page.
		 */
		public function etransfer_vieworder_page($order_id) {
			$order = wc_get_order($order_id);
			if ($order->get_payment_method() == 'moxipay_emt') {
				$uniqueid = get_post_meta( $order->get_id(), '_moxipay_emt_uniqueid', true );
				$instructions = str_replace("{1}", $uniqueid, $this->instructions);
				echo wpautop( wptexturize( $instructions ) );
			}
		}

		/**
		 * Add content to the WC emails.
		 *
		 * @access public
		 * @param WC_Order $order
		 * @param bool $sent_to_admin
		 * @param bool $plain_text
		 */
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
			//if ( $this->instructions && ! $sent_to_admin && 'moxipay_emt' === $order->get_payment_method() && $order->has_status( 'on-hold' ) ) {
			if ( 'moxipay_emt' === $order->get_payment_method() && $order->has_status( 'on-hold' ) ) {
				//$uniqueid = get_post_meta( $order->get_id(), '_moxipay_emt_uniqueid', true );
				//$instructions = str_replace("{1}", $uniqueid, $this->instructions);
                echo wpautop( wptexturize( $autodeposit_desc ) ) . PHP_EOL;
			}
		}
    // Submit payment and handle response
    public function process_payment( $order_id ) {

        global $woocommerce;

        // Get this Order's information so that we know
        // who to charge and how much
        $customer_order = new WC_Order( $order_id );
		$uniqueid = get_post_meta( $order_id, '_moxipay_emt_uniqueid', true );
        $tokenObject = $this->api->getAccessToken(is_ssl());


        if(!is_wp_error($tokenObject)){

        	$data = $_POST;
            $responseInvoice = $this->api->createInvoice($data,$tokenObject->access_token,$order_id,is_ssl());
			
			
            if(!is_wp_error($responseInvoice)){

                // Mark as on-hold (we're awaiting the eTransfer)
                $customer_order->update_status('on-hold', __( 'Awaiting Interac eTransfer (MoxiPay). Ref: ' . $uniqueid, self::TEXT_DOMAIN ));

                // Reduce stock levels
	            wc_reduce_stock_levels($order_id);

                // Remove cart
                $woocommerce->cart->empty_cart();

	            return array(
		            'result' => 'success',
		            'redirect' => $this->get_return_url( $customer_order )
	            );

            }else {

                wc_add_notice( $responseInvoice->get_error_message(), 'error' );
                return null;

            }


        }else{
            wc_add_notice( $tokenObject->get_error_message(), 'error' );
            return null;
        }


    }
	/*
    // Validate fields
    public function validate_fields() {

    	$is_valid = true;
		
        foreach ($this->payment_form_fields as $id=>$field){

	        if ($field['required'] &&  empty( $_POST[$id] ) ){

		        wc_add_notice( "Moxypay EMT <strong>\"{$field['label']}\" </strong> is a required field", 'error' );

	        }
	        if($field['required'] && !empty( $_POST[$id]) && $field['type'] == 'number' &&  !is_numeric($_POST[$id])){

		        wc_add_notice( "Moxypay EMT <strong> \"{$field['label']}\" </strong> must be a number", 'error' );
	        }

        }

		
        return $is_valid;
    }*/

    public function payment_fields() {
		
	    $totalAmount = WC()->cart->get_total();
	    include plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/payment-fields-display.php';
		
    }
	
	
    public function init_payment_fields(){
		
    	$required =  $this->autodeposit == "no";
    	$hideQA =  $this->hide_qa == "yes";


    	$this->payment_form_fields =  [

		    $this->id .'_question' => ['type'=> 'text','label'=> 'Enter the Security Question','required' =>  $required,'value' => '' ],
		    $this->id .'_answer' => ['type'=> 'text','label'=> 'Enter the Security Answer','required' => $required, 'value' => '' ],
		    $this->id .'_note' => ['type'=> 'text','label'=> 'If a notes field exists, enter the following code',
		                           'required' => true, 'readonly'=> true, 'value' => substr( uniqid(),0,10 ) ],
	    ];

    	if(
    		$hideQA){

    		unset($this->payment_form_fields[$this->id .'_question']);
    		unset($this->payment_form_fields[$this->id .'_answer']);

	    }

	    $this->payment_form_fields = apply_filters('moxypay_emt_add_payment_fields',$this->payment_form_fields, $this->id);
		
    }
	
	
	// Build the administration fields for this specific Gateway
	public function init_form_fields() {

		$this->form_fields =  array(
			'enabled' => array(
				'title'		=> __( 'Enable / Disable', self::TEXT_DOMAIN ),
				'label'		=> __( 'Enable this payment gateway', self::TEXT_DOMAIN ),
				'type'		=> 'checkbox',
				'default'	=> 'no',
				'description' => '<strong>IPN url: </strong>' .str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'wc_gateway_moxipay_emt_ipn', home_url( '/' ) ) )
			),
			'recipient_email' => array(
				'title'		=> __( 'Recipient Email', self::TEXT_DOMAIN ),
				'type'		=> 'text',
				'desc_tip'	=> __( 'Who will receive emt.', self::TEXT_DOMAIN ),
				'default'	=> __( ' transfer@moxipay.com', self::TEXT_DOMAIN ),
			),
			'email_notifications' => array(
				'title'		=> __( 'Email Notifications', self::TEXT_DOMAIN ),
				'type'		=> 'text',
				'desc_tip'	=> __( 'For example if emt amount is different than expected (Order Total), a notification will be sent to this email.', self::TEXT_DOMAIN ),
				'default'	=> __( '', self::TEXT_DOMAIN ),
			),			
			'title' => array(
				'title'		=> __( 'Title', self::TEXT_DOMAIN ),
				'type'		=> 'text',
				'desc_tip'	=> __( 'Payment title the customer will see during the checkout process.', self::TEXT_DOMAIN ),
				'default'	=> __( 'MoxiPay EMT', self::TEXT_DOMAIN ),
			),
			'description' => array(
				'title'		=> __( 'Description', self::TEXT_DOMAIN ),
				'type'		=> 'textarea',
				'desc_tip'	=> __( 'Payment description the customer will see during the checkout process.', self::TEXT_DOMAIN ),
				'default'	=> __( 'The safe, secure, fast online payment service.', self::TEXT_DOMAIN )
			),
			'instructions' => array(
				'title'       => __( 'Instructions', 'woocommerce' ),
				'type'        => 'textarea',
				'description' => __( 'Default instructions that will be added to the thank you page and emails.', 'woocommerce' ),
				'default'     => '',
				'desc_tip'    => true,
			),
			'client_id' => array(
				'title'		=> __( 'MoxiPay Client ID', self::TEXT_DOMAIN ),
				'type'		=> 'text',
				'desc_tip'	=> __( 'This is the API Client ID provided by MoxiPay.', self::TEXT_DOMAIN ),
			),
			'client_secret' => array(
				'title'		=> __( 'MoxiPay Client Secret', self::TEXT_DOMAIN ),
				'type'		=> 'text',
				'desc_tip'	=> __( 'This is the API Client Secret provided by MoxiPay.', self::TEXT_DOMAIN ),
			),
			'username' => array(
				'title'		=> __( 'MoxiPay Username', self::TEXT_DOMAIN ),
				'type'		=> 'text',
				'desc_tip'	=> __( 'This is the Username provided by MoxiPay.', self::TEXT_DOMAIN ),
			),
			'password' => array(
				'title'		=> __( 'MoxiPay Password', self::TEXT_DOMAIN ),
				'type'		=> 'password',
				'desc_tip'	=> __( 'This is the Password provided by MoxiPay.', self::TEXT_DOMAIN ),

			),'auto_approve_order_type' => array(
				'title'		=> __( 'Auto-Approve Orders Type', self::TEXT_DOMAIN ),
				'options' => array(
					'' => 'Select',
					'fixed' => 'Fixed Amount',
					'percentage' => '%',
				),
				'type'		=> 'select',
				'css'       => 'width: 100px;padding: 1px',
				'description' => ''

			),'auto_approve_order_amount' => array(
				'title'		=> __( 'Auto-Approve Orders Amount', self::TEXT_DOMAIN ) ,
				'css'       => 'width: 100px;',
				'type'		=> 'text',
				'description' => 'Automatically marks orders as "processing" when payment received is within margins'
			),
			
			'default_secret_answer' => array(
				'title'		=> __( 'Default Secret Answer', self::TEXT_DOMAIN ),
				'type'		=> 'text',
				'description' => 'Display this text value (if entered) as a secret answer in the thank you page'
			),

			'hide_qa' => array(
				'title'		=> __( 'Hide  Q&A fields', self::TEXT_DOMAIN ),
				'label'		=> __( 'Hide / Show', self::TEXT_DOMAIN ),
				'type'		=> 'checkbox',
				'default'	=> 'no',
				'description' => 'Hide Q&A fields.'
			),
			'autodeposit' => array(
				'title'		=> __( 'AutoDeposit', self::TEXT_DOMAIN ),
				'label'		=> __( 'Enable / Disable', self::TEXT_DOMAIN ),
				'class'		=> 'autodeposit-element',
				'type'		=> 'hidden',
				'default'	=> 'yes',
				'description' => 'Active by Default'

			),'autodeposit_desc' => array(
				'title'		=> __( 'Autodeposit Instructions. You can use the following fields in this editor: {recipient_email}, {uniqueid} and {answer}. These tags will be replaced dynamically.', self::TEXT_DOMAIN ),
				'type'		=> 'editor',
				'default'	=> '<h2>Payment Instructions</h2>
				<strong>Send</strong> an Interac E-transfer (Email Money Transfer) with the following info <strong>AND</strong> click Proceed To Moxipay EMT below to complete your order:
				
				<strong>Recipient Email:</strong>   {recipient_email}
				
				The payment will be to Moxipay Corporation Inc..
				
				<strong> IF </strong>you are not asked for a security question and answer and Moxipay Corporation is the company then continue as prompted by banking instructions.
				
				<strong>IF </strong>you are asked to a security question and answer please enter:
				
				<strong>Message:   </strong>Unique ID: <strong>  {uniqueid}</strong>
				<em>Please make sure to add this number as it is how we recognize you.</em>
				
				<strong> Secret Question: </strong> Use Any Question
				
				<strong>Secret Answer:  {answer}</strong>
				
				&nbsp;',
				'desc_tip' => __( 'Description Autodeposit' ),
			),
			'environment' => array(
				'title'		=> __( 'MoxiPay EMT Test Mode', self::TEXT_DOMAIN ),
				'label'		=> __( 'Enable Test Mode', self::TEXT_DOMAIN ),
				'type'		=> 'checkbox',
				'description' => __( 'Place the payment gateway in test mode.', self::TEXT_DOMAIN ),
				'default'	=> 'no',
			),
			'debug' => array(
				'title'       => __( 'Debug Log', 'woocommerce' ),
				'type'        => 'checkbox',
				'label'       => __( 'Enable logging', 'woocommerce' ),
				'default'     => 'no',
				'description' => sprintf( __( 'Log MoxiPay EMT events, such as IPN requests, inside <code>%s</code>', 'woocommerce' ), wc_get_log_file_path( 'moxipay_emt' ) )
			),
		);

	}

	public function generate_editor_html( $key, $data ) {

		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		$data = wp_parse_args( $data, $defaults );

		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<?php echo $this->get_tooltip_html( $data ); ?>
				<label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>

					<?php wp_editor( htmlspecialchars_decode($this->get_option( $key ) ), esc_attr( $field_key ), $settings = array('textarea_name'=>esc_attr( $field_key )) ); ?>

					<?php echo $this->get_description_html( $data ); ?>
				</fieldset>
			</td>
		</tr>
		<?php



		return ob_get_clean();
	}

    public static function log( $message ) {
        if ( self::$log_enabled ) {
            if ( empty( self::$log ) ) {
                self::$log = new WC_Logger();
            }
            self::$log->add( 'MoxiPay EMT', $message );
        }
    }

    public function format_list_of_items( $items ) {
        $item_string = array();
		$key = 0;
        foreach ( $items as $item ) {
            $item_string [] = $item['name']; 
        } 
		
        return implode(', ',$item_string);
    }
    // Check if we are forcing SSL on checkout pages
    // Custom function not required by the Gateway
    public function do_ssl_check() {
        if( $this->enabled == "yes" ) {
            if( get_option( 'woocommerce_force_ssl_checkout' ) == "no" ) {
                echo "<div class=\"error\"><p>". sprintf( __( "<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>" ), $this->method_title, admin_url( 'admin.php?page=wc-settings&tab=checkout' ) ) ."</p></div>";
            }
        }
    }
}