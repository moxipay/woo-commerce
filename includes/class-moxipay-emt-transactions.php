<?php
/**
 * Created by PhpStorm.
 * User: e-36
 * Date: 1/11/2018
 * Time: 2:46 PM
 */

class MoxipayEmtTransactions {



	public function get_transactions($current_page, $per_page,$where='',$having = ''){


		global $wpdb;

		$meta_keys = array('_payment_method','_moxipay_emt_id','_moxipay_emt_actual_amount'
		,'_moxipay_emt_uniqueid','_moxipay_emt_fee','_moxipay_emt_customername','_moxipay_emt_customeremail','_moxipay_emt_status','_moxipay_emt_date','_order_total');
		$placeholders = array_fill(0, count($meta_keys), '%s');
		$meta_keys_st = implode(', ',$placeholders);

		$query_str = "
				SELECT SQL_CALC_FOUND_ROWS $wpdb->posts.ID as invoiceid,$wpdb->posts.post_status,$wpdb->posts.post_date as date_placed,
				(SELECT GROUP_CONCAT(CONCAT(f.meta_key,'=',f.meta_value) SEPARATOR '||') FROM $wpdb->postmeta f 
				WHERE $wpdb->posts.ID = f.post_id AND f.meta_key IN($meta_keys_st)) AS meta_key 
				FROM $wpdb->posts				
			  	INNER JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id
				WHERE  $wpdb->posts.post_type = %s AND $wpdb->postmeta.meta_key = %s AND $wpdb->postmeta.meta_value = %s  
			  	$where            
                GROUP BY $wpdb->posts.ID
                $having 
				ORDER BY $wpdb->posts.post_date DESC
				LIMIT %d OFFSET %d
			";

		$params = $meta_keys;
		$params[] = 'shop_order';
		$params[] = '_payment_method';
		$params[] = 'moxipay_emt';

		$params[] = $per_page;
		$params[] = $current_page;


		$sql = $wpdb->prepare( $query_str, $params );
		$sql = $wpdb->remove_placeholder_escape($sql);
		//print_r($sql);die;
		$wpdb->get_row( "SET SESSION group_concat_max_len = 3000;" );
		$pageposts = $wpdb->get_results( $sql, OBJECT );
		$total = $wpdb->get_row( "SELECT FOUND_ROWS() as count",OBJECT);
		//echo '<pre>';
		foreach ($pageposts as $key=>&$pagepost){

			$meta_keys_column = explode('||',$pagepost->meta_key);

			foreach ($meta_keys_column as $item){

				$key_value = explode('=',$item);
				$pagepost->{$key_value[0]} = $key_value[1];

			}

			unset($pagepost->meta_key);

			/*if(!isset($pagepost->_moxipay_emt_status)){
				unset($pageposts[$key]);
			}*/
		}


		$result = new stdClass();

		$result->data = $pageposts;
		$result->count = $total->count;

		return $result;
	}

	public function get_filters($filter_vars){

		global $wpdb;
		$extra_param = array();
		$extra_param_having = array();
		$filter = '';
		$having = '';

		if(isset($filter_vars['date_from']) && !empty($filter_vars['date_from'])){

			$filter .= " AND $wpdb->posts.post_date >= %s " ;
			$extra_param[] = sanitize_text_field($filter_vars['date_from']);
		}

		if(isset($filter_vars['date_to']) && !empty($filter_vars['date_to'])){

			$filter .= " AND $wpdb->posts.post_date <= %s ";
			$extra_param[] = sanitize_text_field($filter_vars['date_to']);

		}if(isset($filter_vars['post_status']) && !empty($filter_vars['post_status'])){

			$filter .= " AND $wpdb->posts.post_status = %s ";
			$extra_param[] = sanitize_text_field($filter_vars['post_status']);
		}

		if(isset($filter_vars['text_search']) && !empty($filter_vars['text_search'])){

			$text_search = $wpdb->esc_like(sanitize_text_field( $filter_vars['text_search']) );
			$having = " HAVING $wpdb->posts.ID  LIKE %s OR meta_key LIKE %s";

			$extra_param_having[] = "%".$text_search ."%";
			$extra_param_having[] = "%".$text_search ."%";

		}

		$sql_filter = $wpdb->prepare( $filter, $extra_param );
		$sql_having = $wpdb->prepare( $having, $extra_param_having );

		$data = new stdClass();
		$data->where = $sql_filter;
		$data->having = $sql_having;

		return $data;

	}

	public static function get_columns() {

		$columns = array(
			'invoiceid' => __( 'Invoice', 'woocommerce' ),
			'_order_total'       => __( 'Order Amount', 'woocommerce' ),
			'_moxipay_emt_actual_amount'       => __( 'Actual Amount', 'woocommerce' ),
			'_moxipay_emt_fee'  => __( 'Fees', 'woocommerce' ),
			'post_status'  => __( 'Order Status', 'woocommerce' ),
			'_moxipay_emt_status'  => __( 'Status', 'woocommerce' ),
			'_moxipay_emt_customeremail' => __( 'Customer Email', 'woocommerce' ),
			'date_placed'      => __( 'Order Date', 'woocommerce' ),
			'_moxipay_emt_date'      => __( 'Date  processed', 'woocommerce' ),
		);

		return $columns;
	}
}