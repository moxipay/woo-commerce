<?php

/**
 * Fired during plugin activation
 *
 * @link       ecreations.net
 * @since      1.0.0
 *
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/includes
 */

/**
 *
 * @since      1.0.0
 * @package    Moxipay_Payment_Gateway
 * @subpackage Moxipay_Payment_Gateway/includes
 * @author     ecreations <josel@ecreations.net>
 */
class Moxipay_Emt_Gateway_IPN {

	public static $REJECTED = 'rejected';
	public static $SUCCESS = 'accepted';
	public static $INVALID = 'invalidanswer';
	const TEXT_DOMAIN = 'moxipay-emt-gateway';
	private $debug;
	private $settings;
	private $api;

	public function __construct($debug = false,$settings,$api) {

		$this->debug = $debug;
		$this->settings = $settings;
		$this->api = $api;

		add_action( 'woocommerce_api_wc_gateway_moxipay_emt_ipn', array( $this, 'check_ipn_response' ) );

	}
	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public  function check_ipn_response() {

		$json_data = file_get_contents("php://input");
		$post_data = json_decode($json_data,true);

		if(json_last_error() === JSON_ERROR_NONE){
			
			
			//No need for wc_seq_order_number	52				
			//Added by Erick Creative Computer Consulting, Oct 22,2018
			//$order_id = wc_seq_order_number_pro()->find_order_by_order_number( sanitize_text_field($post_data['invoiceID']) );
			$invoice_id = sanitize_text_field($post_data['invoiceID']);
			//Stable way to retrieve invoice id from moxipay api		
			$order_id = sanitize_text_field($post_data['invoiceID']);

			$actual_amount = $post_data['actualamount'];
			$fee = $post_data['calculatedfee'];
			$customername = $post_data['customername'];
			$customeremail = $post_data['customeremail'];
			$uniqueId = $post_data['uniqueid'];
			$status = $post_data['result'];
		}


		if($this->debug){

			WC_Moxipay_EMT_Gateway::log( 'IPN: ' .$json_data);
		}

		$order = new WC_Order( $order_id );
		
		// first payment, on par, id verified
		
		// first payment, over, id verified
		
		// first payment, short, id verified
		
		// first payment, on par, no id
		
		// first payment, over, no id
		
		// first payment, short, no id
		
		// additional payment, on par, id verified
		
		// additional payent, over, id verified, already processing
		
		// additional payment, still short, id verified
		
		// additional payment, on par, no id
		
		// additional payent, over, no id
		
		// additional payment, still short, no id

		if($order->get_id()){

			$order_total = $order->get_total();
			$order_payments = get_post_meta($order_id,'_moxipay_emt_payments', true);
			$order_uniqueId = get_post_meta($order_id,'_moxipay_emt_uniqueid', true);
			// Get the user ID
			$user_id = get_post_meta($order_id, '_customer_user', true);


			if(self::$SUCCESS == $status && $uniqueId == $order_uniqueId ){

				$order_total_payments = $this->get_order_total_payments($order_payments);
				//Actual payment and order amount are equal
				if ( ( $actual_amount + $order_total_payments ) == $order_total || $this->auto_approve_order( ( $actual_amount + $order_total_payments ), $order_total ) ) {

					// Mark order as Paid
					//$order->payment_complete();
					// Payment has been successful
					$order->add_order_note( __( "MoxiPay EMT payment: ". $actual_amount , self::TEXT_DOMAIN ) );
					$order->add_order_note( __( 'MoxiPay EMT payment completed.', self::TEXT_DOMAIN ) );
					//Added by Erick Creative Computer Consulting, Oct 22,2018		
					$order->update_status('processing');

				}
				//Actual payment is grater than order amount
				elseif(($actual_amount + $order_total_payments) > $order_total){
					

					//$order->update_status('on-hold', __( 'MoxiPay EMT payment', self::TEXT_DOMAIN ));
					$order->add_order_note( __( "MoxiPay EMT payment: $ ". number_format( $actual_amount, 2, '.', ',') , self::TEXT_DOMAIN ) );
					$order->add_order_note( __( 'MoxiPay EMT: Actual amount is Higher than order total.', self::TEXT_DOMAIN ) );
					$order->update_status('processing');
					$this->send_email_when_amount_different_than_expected( $order_id, $order_total, $actual_amount, $order_uniqueId, $customername, $customeremail, $user_id );

				}
				//Actual payment is less than order amount
				elseif(($actual_amount + $order_total_payments) < $order_total){

					//$order->update_status('on-hold', __( 'MoxiPay EMT payment', self::TEXT_DOMAIN ));
					$order->add_order_note( __( "MoxiPay EMT payment: $ ". number_format( $actual_amount, 2, '.', ',') , self::TEXT_DOMAIN ) );
					$order->add_order_note( __( 'MoxiPay EMT: Actual amount is Lower than order total.', self::TEXT_DOMAIN ) );
					$this->send_email_when_amount_different_than_expected( $order_id, $order_total, $actual_amount, $order_uniqueId, $customername, $customeremail, $user_id );
				}

				$this->set_order_payment( $order_id, $order_payments, $actual_amount );
				

			}else if((self::$REJECTED == $status || self::$INVALID ==  $status) && $uniqueId == $order_uniqueId)	{

				$order->update_status('failed', __( 'MoxiPay EMT payment error.', self::TEXT_DOMAIN ));
			}

			$this->api->remove_order_meta($order);

			$actual_amount_meta = get_post_meta($order_id,'_moxipay_emt_actual_amount',true);
			$fee_meta = get_post_meta($order_id,'_moxipay_emt_fee',true);

			if(is_numeric($actual_amount_meta)){

				$actual_amount += $actual_amount_meta;
			}
			if(is_numeric($fee_meta)){

				$fee += $fee_meta;
			}

			update_post_meta($order_id,'_moxipay_emt_actual_amount', $actual_amount);
			update_post_meta($order_id,'_moxipay_emt_fee', $fee);
			update_post_meta($order_id,'_moxipay_emt_date', date('Y-m-d H:i:s'));
			update_post_meta($order_id,'_moxipay_emt_status', $status);

		}

		return true;
	}

	public function auto_approve_order($actualAmount, $orderAmount){

		$approve = false;

		$type = isset( $this->settings['auto_approve_order_type'] ) ? $this->settings['auto_approve_order_type'] : false;
		$amount = is_numeric( $this->settings['auto_approve_order_amount'] ) ? $this->settings['auto_approve_order_amount'] : false;

		if($type && $amount){

			switch ($type){

				case 'fixed':
					$approve = abs($orderAmount - $actualAmount) <= $amount;
					break;
				case 'percentage':
					//Changed from "<=" to "="
					$approve = abs($orderAmount - $actualAmount) == $orderAmount * $amount/100;
					break;
			}

		}

		return $approve;

	}

	public function set_order_payment($order_id,$payments,$amount){


		$payments[] = ['amount'=> $amount,'date_posted' => date('F j, Y, g:i a')];

		update_post_meta($order_id,'_moxipay_emt_payments',$payments);


	}

	public function get_order_total_payments($payments){

		$total = 0;

		if(is_array($payments)){

			foreach ($payments as $payment){

				if(isset($payment['amount']) && $payment['amount'] > 0 )
					$total+= $payment['amount'];
			}
		}


		return $total;
	}

	public static function format_response($post){

		$message = '';
		foreach($post as $k=>$var){
			$message .= "{$k}:{$var}|";
		}
		return $message;
	}

	public function send_email_when_amount_different_than_expected( 
			$order_id, 
			$expected_amount, 
			$actual_amount, 
			$unique_id,
			$customer_name,
			$customer_email,
			$user_id
		){


		$email_notifications = isset( $this->settings['email_notifications']) ? $this->settings['email_notifications'] : false;

		if ( $email_notifications ){


			$mailer = WC()->mailer();
			$recipient = $email_notifications;
			$subject = __("Amount Mis-Match Order # " . sprintf( "%06d" , $order_id), '');
			$content = '
			<p>Amount Mis-Match!</p>
			<table>
				<tr>
					<td>Order Number: ' . sprintf( "%06d" , $order_id) . '</td>
				</tr>
				<tr>
					<td>Expected Amount: ' . number_format( $expected_amount, 2, '.', ',') . '</td>
				</tr>
				<tr>
					<td>Actual Amount: ' . number_format( $actual_amount, 2, '.', ',') . '</td>
				</tr>
				<tr>
					<td>Unique ID: ' . $unique_id . '</td>
				</tr>
				<tr>
					<td>Customer Name: ' . $customer_name . '</td>
				</tr>
				<tr>
					<td>Customer Email: ' . $customer_email . '</td>
				</tr>
				<tr>
					<td>User ID: ' . $user_id . '</td>
				</tr>
			</table>';
			$headers = "Content-Type: text/html\r\n";
			//send the email through wordpress
			$mailer->send( $recipient, $subject, $content, $headers );
		}
	}
	
	/*
	public function get_custom_email_html( $order,$client_email,$amount_deposited, $heading = true, $mailer ) {
		$template = dirname( __FILE__ ) . 'emails/new_payment_received.php';
		return wc_get_template_html( $template, array(
			'order' => $order,
			'client_email' => $client_email,
			'amount_deposited' => $amount_deposited,
			'email_heading' => $heading,
			'sent_to_admin' => false,
			'plain_text'    => false,
			'email'         => $mailer
		) );
	}
	*/

}
