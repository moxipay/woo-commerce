<?php

/**
 * Fired during plugin activation
 *
 * @link       http://ecreations.net/
 * @since      1.0.0
 *
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/includes
 * @author     ecreations.net <dev@ecreations.net>
 */
class Moxipay_Emt_Gateway_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
