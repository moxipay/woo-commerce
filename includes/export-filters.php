<form id="post-filter-" action="" method="get">
	<div class="alignleft actions bulkactions">
		<label for="daterange-actions-picker"  class="">Filter:</label>
		<input type="search" name="text_search" value="<?php echo $_GET['text_search'] ?>" id="text_search"/>
		<label for="daterange-actions-picker" class=" ">Order Status:</label>
		<select name="post_status" id="post_status" style="float: none">
			<option value=""></option>
			<?php   $woo_status = wc_get_order_statuses();

			foreach ($woo_status as $key=>$status):  ?>
				<option value="<?php echo $key  ?>" <?php if($key == $_GET['post_status']) echo 'selected'  ?>><?php echo $status  ?></option>
			<?php endforeach; ?>
		</select>
		<label for="daterange-actions-picker" class=" ">Date From:</label>
		<input type="text" name="date_from" value="<?php echo $_GET['date_from'] ?>" id="date_from" placeholder="YYYY-MM-DD" class="datepicker"/>
		<label for="daterange-actions-picker" class=" ">Date To:</label>
		<input type="text" name="date_to" value="<?php echo $_GET['date_to'] ?>" id="date_to" placeholder="YYYY-MM-DD" class="datepicker" />

		<input type="hidden" name="page" value="wc-reports"  />
		<input type="hidden" name="tab" value="moxipay_emt"  />
		<?php submit_button('Filter', 'action', 'dodate', false); ?>
		<?php submit_button('Export as CSV', 'action', 'doexport', false); ?>

		<div class="export-csv-continer">


			<input type="hidden" id="folder_name" value="<?php echo $upload_folder ?>">

			<table id="ullist" width="50%">
				<?php if(!empty($cron_jobs)):  ?>
				<tr>
					<td><strong>Reports:</strong></td>
				</tr>
				<?php endif; ?>
				<?php

				foreach ($cron_jobs as $filename=>$job): ?>
					<?php if($job['finished'] == true):
						$percent = ( $job->total > 0)?ceil($job->page / $job->total * 100):0;
						$labelID = str_replace('.','',$filename);
						?>
						<tr>
							<td width="50%" id="afile-<?php echo $labelID  ?>"><?php
								if($job->page == $job->total) {
									$path = $upload_folder . '/' . $filename .'.csv';
									echo '<a  href="'. $path .'">' . $filename. '.csv</a>';

								}else{
									echo $filename;
								}

								?></td>


							<td width="10%"><a class="removeFile" data-filename="<?php echo $filename  ?>"><i title="Remove" class="dashicons  dashicons-trash"></i></a></td>


						</tr>
					<?php endif; ?>

				<?php endforeach; ?>
			</table>
		</div>
	</div>
</form>