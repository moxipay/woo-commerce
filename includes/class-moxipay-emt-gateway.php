<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://ecreations.net/
 * @since      1.0.0
 *
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/includes
 * @author     ecreations.net <dev@ecreations.net>
 */
class Moxipay_Emt_Gateway {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Moxipay_Emt_Gateway_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'moxipay-emt-gateway';

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Moxipay_Emt_Gateway_Loader. Orchestrates the hooks of the plugin.
	 * - Moxipay_Emt_Gateway_i18n. Defines internationalization functionality.
	 * - Moxipay_Emt_Gateway_Admin. Defines all hooks for the admin area.
	 * - Moxipay_Emt_Gateway_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */

		require_once plugin_dir_path( dirname( __FILE__ ) ) .'vendor/autoload.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-moxipay-emt-gateway-loader.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-moxipay-api.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wc-moxipay-emt-ipn.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-moxipay-emt-gateway-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-moxipay-emt-gateway-public.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-moxipay-emt-transactions.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wc-moxipay-emt-report-transactions.php';

		$this->loader = new Moxipay_Emt_Gateway_Loader();

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Moxipay_Emt_Gateway_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'plugins_loaded', $plugin_admin, 'moxipay_emt_init',0);
		$this->loader->add_filter( 'plugin_action_links__' . plugin_basename(__FILE__), $plugin_admin, 'wc_moxipay_emt_action_links' );
		$this->loader->add_action("do_meta_boxes", $plugin_admin, 'payments_meta_boxes');
		$this->loader->add_filter( 'woocommerce_admin_order_actions', $plugin_admin, 'add_order_actions',10,2 );
		$this->loader->add_action( 'wp_ajax_woocommerce_resend_moxipay_emt', $plugin_admin, 'resend_order' );
		$this->loader->add_action( 'admin_post_moxipay_emt_transactions', $plugin_admin, 'get_transactions' );
		$this->loader->add_filter( 'woocommerce_admin_reports', $plugin_admin, 'add_report_tab');

		$this->loader->add_action( 'wp_ajax_moxipay_emt_csv_export', $plugin_admin, 'update_csv_file'   );
		$this->loader->add_action( 'wp_ajax_moxipay_emt_csv_export_merge',$plugin_admin, 'csv_export_merge'   );
		$this->loader->add_action( 'wp_ajax_moxipay_emt_remove_csv_export',$plugin_admin, 'remove_csv_export'  );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Moxipay_Emt_Gateway_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Moxipay_Emt_Gateway_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
