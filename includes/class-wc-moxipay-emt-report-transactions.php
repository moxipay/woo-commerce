<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


class Moxipay_Emt_Report_Transactions extends WP_List_Table {

	/**
	 * Max items.
	 *
	 * @var int
	 */
	protected $max_items;
	private $per_page;

	/**
	 * Constructor.
	 */
	public function __construct() {

		parent::__construct( array(
			'singular'  => 'transaction',
			'plural'    => 'transactions',
			'ajax'      => false,
		) );

		$this->per_page = 20;

	}

	/**
	 * No items found text.
	 */
	public function no_items() {
		_e( 'No transactions found.', 'woocommerce' );
	}

	/**
	 * Don't need this.
	 *
	 * @param string $position
	 */
	public function display_tablenav( $position ) {

		//if ( 'top' == $position ) {
			parent::display_tablenav( $position );
		//}
	}

	public function extra_tablenav($which)
	{
		if ( 'top' == $which ){

			$user_id       = get_current_user_id();
			$upload_dir    = wp_upload_dir();
			$upload_folder = $upload_dir['baseurl'];
			$cron_jobs = get_option( 'moxipay_emt_' . $user_id, array() );

			if(is_array($cron_jobs)){

				foreach ($cron_jobs as &$job){

					if(!isset($job['finished'])){
						unset($job);
					}
				}

				update_option('moxipay_emt_' . $user_id,$cron_jobs);
			}else{

				$cron_jobs = array();
			}



			include 'export-filters.php';
        }
	}

	/**
	 * Output the report.
	 */
	public function output_report() {

		$this->prepare_items();
		echo '<div id="poststuff" class="woocommerce-reports-wide">';
		$this->display();
		echo '</div>';


	}

	function column_default( $item, $column_name ) {
		return $item->{$column_name} ;
	}

	/**
	 * Get column value.
	 *
	 * @param mixed $item
	 * @param string $column_name
	 */
	public function column_invoiceid( $item ) {


		printf(
			'<a class="" href="%1$s" >%2$s</a>',
			admin_url( 'post.php?post=' . $item->invoiceid . '&action=edit' ),
			esc_html( '#'.$item->invoiceid )
		);
	}

	public function column__moxipay_emt_status( $item ) {


		printf(
			'<span class="report-status-%1$s">%1$s</span>',
			esc_html( $item->_moxipay_emt_status )
		);
	}

	public function column_date_placed( $item ) {

		 return date('M j, Y g:i', strtotime($item->date_placed));
	}

	public function column__order_total( $item ) {

		 return wc_price($item->_order_total);
	}

	public function column__moxipay_emt_fee( $item ) {

		if(!empty($item->_moxipay_emt_fee)){

			return wc_price($item->_moxipay_emt_fee);

        } else{

		    return '';
        }

	}

	public function column__moxipay_emt_actual_amount( $item ) {

		if(!empty($item->_moxipay_emt_actual_amount)){

			return wc_price($item->_moxipay_emt_actual_amount);

        } else{

		    return '';
        }

	}

	public function column__moxipay_emt_date( $item ) {

		if(isset($item->_moxipay_emt_date) && !empty($item->_moxipay_emt_date)){

		    return date('M j, Y g:i', strtotime($item->_moxipay_emt_date));
        }



	}

	public function column_post_status( $item ) {

	      $woo_status = wc_get_order_statuses();
		 return  $woo_status[$item->post_status];
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	public function get_columns() {

		$columns = MoxipayEmtTransactions::get_columns();

		return $columns;
	}

	/**
	 * Prepare customer list items.
	 */
	public function prepare_items() {

		$this->_column_headers = array( $this->get_columns(), array(), $this->get_sortable_columns() );
		$current_page          = absint( $this->get_pagenum() );
		$per_page              =  $this->per_page;


		$this->get_items( $current_page, $per_page );


		/**
		 * Pagination.
		 */
		$this->set_pagination_args( array(
			'total_items' => $this->max_items,
			'per_page'    => $per_page,
			'total_pages' => ceil( $this->max_items / $per_page ),
		) );

	}

	public function get_items( $current_page, $per_page ){


	    $moxiPayDB = new MoxipayEmtTransactions();
		$sql_filter = $moxiPayDB->get_filters($_GET);

		if ( empty( $current_page ) || ! is_numeric( $current_page ) || $current_page <= 1 ) {
			$current_page = 0;
		}else{

			$current_page = $current_page -1;
		}

		$pageposts = $moxiPayDB->get_transactions($current_page, $per_page,$sql_filter->where,$sql_filter->having);

		//print_r($pageposts);die;

		$this->max_items =  $pageposts->count;
		$this->items = $pageposts->data;

    }


}
