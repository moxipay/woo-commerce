var cron_queue = [];

(function( $ ) {
	'use strict';

    $(document).ready(function(){



        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd'
        });

        $("#doexport").click(function(e) {


                e.preventDefault();

                //jQuery('.export-csv-continer').remove();

                var text_search = $("#text_search").val();
                var today = new Date();
                var date = today.getFullYear()+''+(today.getMonth()+1)+''+today.getDate();
                var time = today.getHours() + "" + today.getMinutes() + "" + today.getSeconds();
                var dateTime = date + '_' + time;
                var csvfile_name =   'moxipay-emt-transactions-'+  dateTime + '-export';
                var csvfile_nameid = csvfile_name.replace('.','');
                var filelabel = csvfile_name;
                var td_name = $('<td width="60%" id="afile-'+csvfile_nameid+'">').append(filelabel + '.csv');
                var td_bar = $('<td>').append('<div class="w3-progress-container"> <div id="pb-'+ csvfile_nameid +'" data-status="active" class="w3-progressbar w3-green" >  ' +
                    '<div class="w3-center w3-text-white">0%</div></div></div>');
                var td_remove = $('<td width="10%">').append('<a class="removeFile" data-filename="'+ csvfile_name +'"><i title="Remove" class="dashicons  dashicons-trash"></i></a>');

                var tr = $('<tr>');
                tr.append(td_name);
                tr.append(td_bar);
                tr.append(td_remove);

                jQuery("#ullist").append(tr);


                var filter = {};

                filter.filename =  csvfile_name,
                filter.text_search =  text_search,
                filter.page = 1,
                filter.date_from = jQuery('#date_from').val(),
                filter.date_to = jQuery('#date_to').val(),
                filter.post_status = jQuery('#post_status').val(),

                update_csv_file(filter);

                return false;


            return true;

        });

        $(document.body).off('click', '.removeFile').on("click",'.removeFile', function(e)
        {
            var filename = jQuery(this).data('filename');
            var fiilenameindex = filename.replace('.','');

            var data = {
                'action': 'moxipay_emt_remove_csv_export',
                'filename': filename,
            };

            jQuery.post(ajaxurl, data, function (response) {


            });

            jQuery(this).parent().parent().remove();

            try { cron_queue[fiilenameindex].abort(); } catch(e){ }

        });

        loadActiveJobs();

    });



})( jQuery );


function loadActiveJobs(){

    var post_ids_arr = [];
    jQuery( '#ullist .w3-progressbar' ).each(function() {

        if(jQuery(this).data('status') == 'active'){
            var filter = jQuery(this).data('filter');
            filter.page++;
            update_csv_file(filter);
        }

        post_ids_arr.push( jQuery(this).val());
    });


}
function update_csv_file(filter) {

    var data = {
        'action': 'moxipay_emt_csv_export',
        'text_search': filter.text_search,
        'post_status': filter.post_status,
        'filename': filter.filename,
        'page': filter.page,
        'date_from': filter.date_from,
        'date_to': filter.date_to,
        'last_id': filter.last_id,
        'id': filter.id,
    };
    var fiilenameindex = filter.filename.replace('.','');
    cron_queue[fiilenameindex] = jQuery.post(ajaxurl, data, function (response) {

        var foldername = jQuery('#folder_name').val();

        var fiilenameindex = response.filename.replace('.','');
        var percent = (response.page / (response.total + 1)) * 100;
        percent = Math.ceil(percent);

        jQuery('#pb-'+fiilenameindex).css('width', percent + '%');
        jQuery('#pb-'+fiilenameindex).find('.w3-center').html(percent + '%');

        response.page++;
        filter.last_id = response.last_id;
        filter.id = response.id;

        if (response.page <= response.total) {
            update_csv_file( response );
        }else{

            var data = {
                'action': 'moxipay_emt_csv_export_merge',
                'filename': filter.filename,
                'page': filter.page,
                'id': filter.id,
            };

            jQuery.post(ajaxurl, data, function (response) {

                jQuery('#afile-'+fiilenameindex).html('<a href="'+ foldername+ '/'+filter.filename+'.csv">'+ filter.filename +'.csv</a>')

                jQuery('#pb-'+fiilenameindex).css('width','100%');
                jQuery('#pb-'+fiilenameindex).find('.w3-center').html( '100%');
            });



        }

    }, 'json');
}