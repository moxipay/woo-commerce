<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://ecreations.net/
 * @since      1.0.0
 *
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="woocommerce_order_items_wrapper wc-order-items-editable">
<table class="woocommerce_order_items" cellspacing="0" cellpadding="0">
    <thead>
    <tr>

        <th class="date" data-sort="float">Posted date</th>
        <th class="item"   data-sort="string-ins">Amount</th>
    </tr>
    </thead>

    <tbody id="order_line_items">

    <?php
    $total = 0;
    foreach ($payments as $payment):?>
    <tr class="item">

        <td class="date"><div class="view"><?php echo $payment['date_posted'] ?></div></td>
        <td class="amount"><div class="view"><?php echo wc_price($payment['amount']); $total+= $payment['amount']?></div> </td>
    </tr>
    <?php endforeach; ?>
    </tbody>

</table>

    <div class="wc-order-data-row wc-order-totals-items wc-order-items-editable">

        <table class="wc-order-totals">
            <tbody>


            <tr>
                <td class="label"  >Total:</td>
                <td width="1%"></td>
                <td class="total">
                    <span class="woocommerce-Price-amount amount"><?php echo wc_price($total)?></span>
                </td>
            </tr>




            </tbody>

        </table>

        <div class="clear"></div>
    </div>
</div>