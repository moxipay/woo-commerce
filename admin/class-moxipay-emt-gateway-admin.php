<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://ecreations.net/
 * @since      1.0.0
 *
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Moxipay_Emt_Gateway
 * @subpackage Moxipay_Emt_Gateway/admin
 * @author     ecreations.net <dev@ecreations.net>
 */
class Moxipay_Emt_Gateway_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->cron_job = 'moxipay_emt_';

	}

	public function moxipay_emt_init(){


		if (!class_exists( 'WC_Moxipay_EMT_Gateway' ) ) {

			// Include our Gateway Class and register Payment Gateway with WooCommerce
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wc-moxipay-emt-gateway.php';

			add_filter( 'woocommerce_payment_gateways',array($this, 'wc_add_moxipay_emt_gateway') );
		}
	}

	public function wc_moxipay_emt_action_links( $links ) {

		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">' . __( 'Settings', 'moxipay-payment-gateway' ) . '</a>',
		);

		// Merge our new link with the default ones
		return array_merge( $plugin_links, $links );
	}

	public function wc_add_moxipay_emt_gateway( $methods ) {

		$methods[] = 'WC_Moxipay_EMT_Gateway';

		return $methods;
	}

	public function payments_meta_boxes(){

		add_meta_box( 'woocommerce-order-moxypayemt-payments', __( 'Moxypay EMT Payment Transactions', $this->plugin_name), array($this,'payment_table'), 'shop_order' , 'normal', 'high' );

	}

	public function payment_table(){
		global $post;

		$payments = get_post_meta($post->ID,'_moxipay_emt_payments',true);

		if(!empty($payments)){

			include 'partials/order-payment-display.php';
		}

	}
	public function get_transactions_report( $name ) {
		$name  = sanitize_title( str_replace( '_', '-', $name ) );
		$class = 'Moxipay_Emt_Report_Transactions';

		if ( ! class_exists( $class ) ) {
			return;
		}

		$report = new $class();
		$report->output_report();
	}


	public function add_report_tab($reports){


		$reports['moxipay_emt']  = array(
			'title'  => __( 'Moxipay EMT', 'woocommerce' ),
			'reports' => array(
				"low_in_stock" => array(
					'title'       => __( 'Transactions', 'woocommerce' ),
					'description' => '',
					'hide_title'  => true,
					'callback'    => array( $this, 'get_transactions_report' ),
				)

			),
		);

		return $reports;

	}

	/**
	 * @param $actions
	 * @param $the_order
	 * @return mixed
	 *
	 * Add the export TXT action to Woocomemrce order
	 */
	public function add_order_actions($actions, $order){

		$uniqueid = get_transient('order_'.$order->get_id().'_uniqueid');
		WC_Admin_Notices::remove_notice('ErrorMoxyPay');

		if($uniqueid){

			$actions['export_txt'] = array(
				'url' => wp_nonce_url(admin_url('admin-ajax.php?action=woocommerce_resend_moxipay_emt&order_id=' . $order->get_id()), $this->plugin_name),
				'name' => __('Resend To Moxipay EMT', $this->plugin_name),
				'action' => "resend-moxipay"
			);
		}


		return $actions;
	}

	public function resend_order() {

		if ( ! isset( $_GET['order_id'] ) || ! is_numeric( $_GET['order_id'] ) ) {
			wp_die();
		}

		$order_id = $_GET['order_id'];
		$order    = new WC_Order( $order_id );
		$gatewayId = WC_Moxipay_EMT_Gateway::$gateway_id;
		$settings = get_option('woocommerce_'.$gatewayId.'_settings');

		if(isset($settings['client_id'])){

			$api = new MoxipayApi($gatewayId,$settings['client_id'],$settings['client_secret'],$settings['username'],
													$settings['password'],$settings['environment'],$settings['autodeposit']);

			$tokenObject = $api->getAccessToken(is_ssl());

			if(!is_wp_error($tokenObject)){

				$data = array();
				$data[$gatewayId.'_question'] = get_transient('order_'.$order->get_id().'_question');
				$data[$gatewayId.'_answer'] = get_transient('order_'.$order->get_id().'_answer');
				$data[$gatewayId.'_note'] = get_transient('order_'.$order->get_id().'_uniqueid');

				$responseInvoice = $api->createInvoice($data,$tokenObject->access_token,$order,is_ssl());

				WC_Admin_Notices::remove_notice('ErrorMoxyPay');
				$message = 'Order #'.$order_id.' successfully resend to Moxypay.';

				if(!is_wp_error($responseInvoice)){

					$order->update_status('on-hold', 'Awaiting MoxiPay EMT payment. ');
					$api->remove_order_meta($order);

				}else {
					$message = $responseInvoice->get_error_message();
				}


			}else{

				$message = $tokenObject->get_error_message();
			}

		}

		WC_Admin_Notices::add_custom_notice('ErrorMoxyPay',$message);
		wp_redirect( home_url('/').'wp-admin/edit.php?post_type=shop_order' );
		exit;

	}

	public function update_csv_file(){

		//print_r($_POST);die;

		$file_name = $_POST['filename'];

		$filter                    = new stdClass();
		$filter->filename          = $file_name;
		$filter->parts             = array();
		$filter->last_id           = $_POST['last_id'] ? $_POST['last_id'] : 0;
		$filter->id                = isset( $_POST['id'] ) ? $_POST['id'] : 0;
		$filter->page              = $_POST['page'];
		$post_per_page = 100;
		$user_id       = get_current_user_id();


		try {


			if( (int)$filter->page === 1 ) {
				//delete_option($this->cron_job . $user_id);die;
				$cron_jobs = get_option( $this->cron_job . $user_id, array() );
				$cron_jobs[$file_name] =  $_POST;
				update_option($this->cron_job . $user_id,$cron_jobs);
			}

			$upload_dir    = wp_upload_dir();
			$csv_file      = $upload_dir['basedir'] . '/' . $file_name;

			$moxiPayDB = new MoxipayEmtTransactions();
			$sql_filter = $moxiPayDB->get_filters($_POST);

			if ( empty( $filter->page ) || ! is_numeric( $filter->page ) || $filter->page <= 1 ) {
				$current_page = 0;
			}else{

				$current_page = ($filter->page -1) * $post_per_page;
			}


			$pageposts = $moxiPayDB->get_transactions($current_page, $post_per_page,$sql_filter->where,$sql_filter->having);

			$filter->total = ceil( $pageposts->count/$post_per_page);

			$columns = MoxipayEmtTransactions::get_columns();
			$woo_status = wc_get_order_statuses();

			$rows = array();

			foreach ($pageposts->data as $key=>&$pagepost){

				foreach ($columns as $k_column=>$column){

					switch ($k_column){
						case 'post_status':
							$rows[$key][$k_column] =	$woo_status[$pagepost->{$k_column}];
							break;
						case '_order_total':
							$rows[$key][$k_column] =	html_entity_decode(get_woocommerce_currency_symbol()).$pagepost->{$k_column};
							break;
						case '_moxipay_emt_fee':
							$rows[$key][$k_column] =	(!empty($pagepost->{$k_column}))?html_entity_decode(get_woocommerce_currency_symbol()).$pagepost->{$k_column}:'';
							break;
						default:
							$rows[$key][$k_column] = $pagepost->{$k_column};
							break;

					}

				}

			}



			if ( ! empty( $rows ) ) {

				if ( $filter->page == 1 ) {

					$objPHPExcel = new PHPExcel();
					$objPHPExcel->setActiveSheetIndex( 0 );
					$objPHPExcel->getActiveSheet()->fromArray( array_values($columns)  , null, 'A1' );
					$row = $objPHPExcel->getActiveSheet()->getHighestRow() + 1;
				} else {

					$objPHPExcel = new PHPExcel();
					$objPHPExcel->setActiveSheetIndex( 0 );
					$row = $objPHPExcel->getActiveSheet()->getHighestRow();
				}

				$objPHPExcel->getActiveSheet()->fromArray( $rows, null, 'A' . $row );

				$objWriter = PHPExcel_IOFactory::createWriter( $objPHPExcel, 'CSV' );
				$objWriter->setDelimiter( ',' );
				$objWriter->setSheetIndex( 0 );   // Select which sheet.
				$objWriter->setEnclosure( '"' );

				//ob_start();

				//$objWriter->save( 'php://output' );
				//$csvFileContents = ob_get_clean();

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
				$objWriter->setDelimiter(',');
				$objWriter->setSheetIndex(0);   // Select which sheet.
				$objWriter->setEnclosure('"');
				$objWriter->save($csv_file.'_'.$filter->page.'.csv');


			}

			echo json_encode($filter);

			wp_die();

		} catch ( Exception $ex ) {

			print_r( $ex->getMessage() );
			die;
		}

	}

	public function csv_export_merge(){

		$filename = $_POST['filename'];
		$total = $_POST['page'];
		$user_id       = get_current_user_id();
		$cron_jobs = get_option( $this->cron_job . $user_id, array() );

		$upload_dir    = wp_upload_dir();
		$upload_folder = $upload_dir['basedir'];

		$filepath = $upload_folder . '/' . $filename . '.csv';

		$Open = fopen ($filepath, "w");

		for($i = 1; $i <=  $total ;$i++){

			$Data = file_get_contents ( $upload_folder.'/'.$filename .'_' . $i .  '.csv');
			fwrite ($Open, $Data);
			unlink( $upload_folder.'/'.$filename .'_' . $i .  '.csv');
		}
		fclose ($Open);
		$cron_jobs[ $filename ]['finished'] = true;
		update_option( $this->cron_job . $user_id, $cron_jobs );


		wp_die();

	}

	public function remove_csv_export(){

		$file_name     = $_POST['filename'];
		$user_id       = get_current_user_id();
		$upload_dir    = wp_upload_dir();
		$upload_folder = $upload_dir['basedir'];



		if ( ! empty( $file_name ) ) {
			$cron_jobs = get_option( $this->cron_job . $user_id, array() );

			if ( isset( $cron_jobs[ $file_name ] ) ) {
				unset( $cron_jobs[ $file_name ] );
				update_option( $this->cron_job . $user_id, $cron_jobs );

			}

			unlink( $upload_folder . '/' . $file_name . '.csv' );
		}
		echo 'Ok';
		wp_die();

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/moxipay-emt-gateway-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts( $hook_suffix) {

		if('woocommerce_page_wc-reports' == $hook_suffix){

			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/moxipay-emt-gateway-admin.js', array( 'jquery' ), $this->version, false );

		}



	}

}
