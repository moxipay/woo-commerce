<?php

/**
 *
 * @link              http://ecreations.net/
 * @since             1.6.0
 * @package           Moxipay_Emt_Gateway
 *
 * @wordpress-plugin
 * Plugin Name:       MoxiPay EMT Payment Gateway
 * Plugin URI:        http://www.moxipay.com/
 * Description:       MoxiPay Email Money Transfer Payment Gateway for Woocommerce
 * Version:           1.6.0
 * Author:            ecreations.net
 * Author URI:        http://ecreations.net/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       moxipay-emt-gateway
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_NAME_VERSION', '1.6.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-moxipay-emt-gateway-activator.php
 */
function activate_moxipay_emt_gateway() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-moxipay-emt-gateway-activator.php';
	Moxipay_Emt_Gateway_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-moxipay-emt-gateway-deactivator.php
 */
function deactivate_moxipay_emt_gateway() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-moxipay-emt-gateway-deactivator.php';
	Moxipay_Emt_Gateway_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_moxipay_emt_gateway' );
register_deactivation_hook( __FILE__, 'deactivate_moxipay_emt_gateway' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-moxipay-emt-gateway.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_moxipay_emt_gateway() {

	$plugin = new Moxipay_Emt_Gateway();
	$plugin->run();

}
run_moxipay_emt_gateway();
