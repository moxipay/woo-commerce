<fieldset id="wc-<?php echo esc_attr( $this->id ); ?>-form" class='wc-payment-form'>
	<?php do_action( 'moxypay_emt_form_start', $this->id ); ?>

	<?php if ( $this->autodeposit == "yes" ): ?>

        <?php
	        //$content = htmlspecialchars_decode($this->autodeposit_desc);
            //echo wpautop($content) 
        ?>

	<?php else: ?>

    <ol class="moxypay-emt-instructions">


        <li style="list-style: lower-roman !important"><?php echo $this->step_1 ?></li>
        <li style="list-style: lower-roman !important"><?php echo $this->step_2 ?></li>
        <li style="list-style: lower-roman !important"><?php echo $this->step_3 ?> </li>
        <li style="list-style: lower-roman !important"><?php echo $this->step_4 ?></li>
        <li style="list-style: lower-roman !important"><?php echo $this->step_5 ?></li>


    </ol>
    <?php endif; ?>

    <p><?php echo $this->description; ?></p>

    <!--<p><label> Please ensure information matches exactly EMT </label><p/>

    <p> <label> Amount: <strong><?php echo $totalAmount ?></strong> </label> </p>

    <?php  foreach ($this->payment_form_fields as $id => $field): ?>

    <p class="form-row moxypay-emt-first-row">
        <label for="<?php echo $id?>"><?php echo esc_html__( $field['label'],self::TEXT_DOMAIN ) ?> <?php if($field['required']): ?><abbr class="required" title="required">*</abbr><?php endif; ?></label>
        <input id="<?php $id ?>"  name="<?php echo $id ?>"  class="input-text"  autocomplete="off" spellcheck="no"
               type="<?php echo $field['type'] ?>"  value="<?php echo $field['value'] ?>"  <?php echo ($field['readonly'])?'readonly':'' ?> />
    </p>

    <?php  endforeach; ?>
    -->
	<?php do_action( 'moxypay_emt_form_end', $this->id ); ?>


    <div class="clear"></div>
</fieldset>
